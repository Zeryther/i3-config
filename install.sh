cp ./* ~/.i3/
sudo apt install i3 i3blocks feh pavucontrol fonts-font-awesome maim

# dunst
sudo apt install libdbus-1-dev libx11-dev libxinerama-dev libxrandr-dev libxss-dev libglib2.0-dev libpango1.0-dev libgtk2.0-dev libxdg-basedir-dev
git clone https://github.com/dunst-project/dunst.git
cd dunst
make -j5
sudo make install
cd ../
mv dunst /usr/share/
mkdir ~/.config/ && mkdir ~/.config/dunst/
sudo cp /usr/share/dunst/dunstrc ~/.config/dunst/dunstrc
